/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList,ActivityIndicator,ListItem} from 'react-native';
import axios from "axios";


export default class App extends Component<Props> {
  state = {
    loading:false,
    data:[],
    page:1,
    error:null,
    refresh:false,
  }



  loadData = () => {
     const { data, page } = this.state;

     setTimeout(()=>{
       var url = "https://harptv.org/haberlerGetir?page="+page;
       this.setState({loading:true});
       axios.get(url)
       .then(
         res => {
           console.log(res.data);
           this.setState({
             data : page === 1 ? res.data.data : [...data,...res.data.data],
             loading:false,
             refresh:false
           });
         }
       )
       .catch(error => {
           this.setState({error,loading:false});
       });
     },1500)

  }
  handleRefresh = () => {
     this.setState({
       isRefreshing: true,
     }, () => {
       this.loadData();
     });
   };

   handleLoadMore = () => {
     this.setState({
       page: this.state.page + 1
     }, () => {
       this.loadData();
     });
   };

   renderFooter = () => {


     return (
       <View
       style={{
         paddingVertical: 20,
         borderTopWidth: 1,
         borderColor: "#CED0CE"
       }}
       >
          <ActivityIndicator animating size="large" />
       </View>
     );
   s};

   componentDidMount(){
     this.loadData();
   }
  render() {
    const { data, refresh } = this.state;
    if(refresh){
        return <ActivityIndicator style={{flex:1,justifyContent:'center',alignItems:'center'}}/>;
    }
    else{
    return (
      <View style={{ flex: 1}}>

        {data &&
        <FlatList
          data={data}
          horizontal={false}
          numColumns={1}
          renderItem={ ({item,index}) => (

              <View key={index} style={{flex:1,backgroundColor:"red",margin:20,justifyContent:'center',alignItems:'center'}}>
              <Text>{item.Baslik}</Text>
              </View>


        )}
        keyExtractor={i => i.id}
        refreshing={refresh}
        ListFooterComponent={this.renderFooter}
        onRefresh={this.handleRefresh}
        onEndReached={this.handleLoadMore}
        onEndThreshold={0}
        />}
      </View>
    );
    }
  }


}
